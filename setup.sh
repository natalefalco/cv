#!/bin/bash

# in case local development symlinks are enough
# the CI_JOB variable is only present on the pipeline
if [ -z "${CI_JOB_ID+x}" ]; then
  mkdir public
  ln  ./src/* ./public
else
  cp -r ./src ./public
fi

# the assets used by the page
# secrets is managed by gitlab
# for local development setup the awscli creds
aws s3 cp s3://static.noelsr.dev/ public/ \
    --recursive \
    --exclude=* \
    --include=background.svg \
    --include=preview.png \
    --include=cake.svg \
    --include=email.svg \
    --include=favicon.png \
    --include=favicon.svg \
    --include=home.svg \
    --include=lmmonolt10-regular-webfont.woff \
    --include=lmmonolt10-regular-webfont.woff2 \
    --include=person.svg \
    --include=phone.svg \
    --include=portre_bw.jp2 \
    --include=school.svg \
    --include=oracle-developer-associate.png \
    --include=oracle-architect-associate.png \
    --include=cka-certified-kubernetes-administrator.png \
    --include=cks-certified-kubernetes-security-specialist.png \
    --include=certs/noel-lszl-slyom-rvsz-certified-kubernetes-security-specialist-cks-certificate.pdf \
    --include=certs/noel-lszl-slyom-rvsz-certified-kubernetes-administrator-cka-certificate.pdf \
    --include=certs/Remote_Pilot_Licence_Sólyom-Révész_Noel_László_AUT-RP-u74wxd4ef9v2.pdf \
    --include=certs/oracle_certificate_architect_associate.pdf \
    --include=certs/oracle_certificate_developer_associate.pdf \
    --include=icons/*
