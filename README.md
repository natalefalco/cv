[![Build Status](https://gitlab.com/natalefalco/cv/badges/master/pipeline.svg)](https://gitlab.com/natalefalco/cv/-/pipelines/latest)

[dev page]: https://natalefalco.gitlab.io/cv
[prod page]: https://cv.noelsr.dev

---

# Description

plain CV on HTML site using GitLab Pages.

# TODO:

- Include Diploma copy
- Make the site printable

---
